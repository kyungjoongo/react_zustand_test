class SharedService {

    async getList() {
        let result = await fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response => response.json())
            .then(json => json)

        return result;
    }

}

const sharedService = new SharedService()

export default sharedService;
