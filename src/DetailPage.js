import {useStore} from "./States/Counter";


export default function DetailPage() {

    const {loading, count} = useStore(state => state)

    return (
        <>
            <div>
                detail page
                <div>
                    {count}
                </div>
                <div>
                    {loading &&
                    <div>
                        loading...........
                    </div>
                    }
                </div>
            </div>
        </>
    )
}
