import create from "zustand";

export const useStore = create(set => ({
    count: 1,
    loading: false,
    results: [],
    inc: () => set(state => {
        return (
            {count: state.count + 2}
        )
    }),
    dec: () => set(state => ({count: state.count - 1})),
    setLoading: (value) => set(state => {

        return (
            {loading: value}
        )
    }),
    setResults: (list) => set(state => {
        return (
            {results: list}
        )
    }),
}))
