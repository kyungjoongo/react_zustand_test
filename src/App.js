import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import MainPage from "./MainPage";
import DetailPage from "./DetailPage";
import {withAuthenticator} from '@aws-amplify/ui-react'

function App() {

    return (
        <>
            <Router>
                <Switch>
                    <Route path="/" exact component={MainPage}/>
                    <Route path="/Detail" exact component={DetailPage}/>

                </Switch>
            </Router>
        </>
    )
}

export default withAuthenticator(App);
