import {useStore} from "./States/Counter";
import {useEffect} from "react";
import sharedService from "./Service/SharedService";
import {ActivityIndicator} from 'react-native-web'


export default function MainPage(props) {
    const {inc, dec, setLoading, setResults,} = useStore()
    const {count, loading, results} = useStore(state => state)

    useEffect(() => {
        getInit()
    }, [])

    async function getInit() {
        setLoading(true)
        let result = await sharedService.getList();
        console.info("result====>", result);
        setResults(result)
        setTimeout(() => {
            setLoading(false);
        }, 999)
    }

    return (
        <>
            <button onClick={inc}>up</button>
            <button onClick={dec}>down</button>
            <div>{count}</div>
            <button onClick={() => {
                props.history.push('Detail')
            }}>
                push

            </button>
            {loading &&
            <ActivityIndicator color={'red'}/>
            }
            <button onClick={() => {
                setLoading(!loading)
            }}>
                toggle Loading
            </button>

            {!loading && results.map(item => {
                return (
                    <div>
                        {item.title}
                    </div>
                )
            })}

        </>
    )
}
